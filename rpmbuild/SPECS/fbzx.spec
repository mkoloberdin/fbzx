Name: fbzx
Version: 4.0.0
Release: 1
License: GPL3
Summary: A Sinclair ZX Spectrum emulator

BuildRequires: gcc
BuildRequires: gcc-c++
BuildRequires: glibc-headers
BuildRequires: cmake
BuildRequires: pkgconfig
BuildRequires: make
BuildRequires: SDL2-devel
BuildRequires: pulseaudio-libs-devel
BuildRequires: alsa-lib-devel

Requires: SDL2
Requires: pulseaudio-libs
Requires: spectrum-roms
Requires: alsa-lib

%description
A Sinclair ZX Spectrum emulator
.

%post

%clean
rm -rf %{buildroot}

%build
mkdir -p ${RPM_BUILD_DIR}
cd ${RPM_BUILD_DIR}; cmake -DCMAKE_INSTALL_PREFIX=/usr
make -C ${RPM_BUILD_DIR}

%postun

%files
/*

%install
make install -C ${RPM_BUILD_DIR} DESTDIR=%{buildroot}

